#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <limits>

using namespace std;

void doPause(void);
void dispExplanation(void);
double arithOperations(double, double, char);

int main(int nNumberofArgs, char* pszArgs[])
{
	dispExplanation();
	double dNumber1;
	double dNumber2 = 0;
	char cOperator;
    cout << "Input your number: ";
    cin >> dNumber1;
    for(;;)
    {

        cout << "Choose operator (+, -, /, * or =): ";
        cin >> cOperator;
        if (cOperator == '=')
        {
            cout << endl;
            cout << "Final result is " << dNumber1 << endl;
            dNumber1 = 0;
            break;
        }
        else
        {
            cout << "Input another number: ";
            cin >> dNumber2;
            double dResult;
            dResult = arithOperations(dNumber1, dNumber2, cOperator);
            cout << endl;
            cout << "Result is " << dResult << endl;
            dNumber1 = dResult;
        }
    }
	doPause();
	return 0;
}

void doPause(void)
{
	cout << "Press [ Enter ] to continue..." << endl;
	cin.ignore(numeric_limits<streamsize>::max(), '\n');
	cin.clear();
	cin.get();
}

void dispExplanation(void)
{
    cout << "This is simple (rather foolish) calculator app, "
        << "written on C++ and based on cycles\n";
    cout << endl;
    cout << "First input any number and then choose one of operators\n";
    cout << "(+ means addition, - for subtraction, / for division, * for multiplication and = to display a result AND quit\n";
    cout << endl;
}

double arithOperations(double dNumber1, double dNumber2, char cOperator)
{
    /*if (dNumber2 == 0)
    {
        dNumber2 = dNumber1;
    }
    else*/
    {
        switch (cOperator)
        {
            case '+':
                dNumber1 += dNumber2;
                break;

            case '-':
                dNumber1 -= dNumber2;
                break;

            case '/':
                dNumber1 = dNumber1 / dNumber2;
                break;

            case '*':
                dNumber1 *= dNumber2;
                break;

            case '=':
                cout << "Result is " << dNumber1 << endl;
                dNumber1 = 0;
                break;

            default:
                cout << "not supported" << endl;
                /*exit (EXIT_FAILURE);*/
        }
    }
    return dNumber1;
}
